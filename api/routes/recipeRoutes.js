'use strict';
module.exports = function(app) {
    var recipe = require('../controllers/recipeController');

    // todoList Routes
    app.route('/recipes')
        .get(recipe.getAllRecipes);
};
