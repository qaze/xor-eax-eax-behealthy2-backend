'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var RecipeSchema = new Schema({
    name: {
        type: String,
        required: 'recipe name is required'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    ingredients: {
        type: [{
            name: String,
            amount: Number,
            unit: String
        }]
    },
    calories: {
        type: Number
    },
    categories: {
        type: [String]
    },
    nutrition: {
        type: [{
            name: String,
            amount: Number,
            unit: {
                type: String,
                enum: ['g', 'mg', 'μg']
            }
        }]
    },
    popularity: {
        type: Number,
        default: 0
    },
    instructions: {
        type: String,
    }
});

module.exports = mongoose.model('Recipes', RecipeSchema);