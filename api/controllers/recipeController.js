'use strict';


var mongoose = require('mongoose'),
    Recipe = mongoose.model('Recipes');

exports.getAllRecipes = function (req, res) {
    let query = Recipe.find();
    let promise = query.exec();
    promise.then(function (docs) {
        res.json(docs)
    });
};