var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    Recipe = require('./api/models/recipeModel'); //created model loading here

mongoose.set('bufferCommands', false);

const connectionString = 'mongodb+srv://fridgeMix:3xEkfDi3kAKmzExR@cluster0-zo6m7.gcp.mongodb.net/fridgeMix';

const connector = mongoose.connect(connectionString);

Recipe = mongoose.model('Recipes');

connector.then(
    () => {
        console.log("Database connected");
        let query = Recipe.find();
        let promise = query.exec();
        promise.then(function (docs) {console.log(JSON.stringify(docs))});

    },
    (err) => console.log(err)
);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


let routes = require('./api/routes/recipeRoutes'); //importing route
routes(app); //register the route




app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//mongodb+srv://fridgeMix:or3Vr5u8kx2Dj7sM@cluster0-zo6m7.gcp.mongodb.net/test?retryWrites=true
//mongodb://fridgeMix:or3Vr5u8kx2Dj7sM@cluster0-zo6m7.gcp.mongodb.net/test?retryWrites=true
//mongodb+srv://fridgeMix:<password>@cluster0-zo6m7.gcp.mongodb.net/test?retryWrites=true